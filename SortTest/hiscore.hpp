#include <string>
#include <vector>

class Scoreboard
{
public:
	enum ScoreEntry : unsigned char
	{
		SCORE_A_B_EASY = 0,
		SCORE_A_B_NORMAL,
		SCORE_A_B_HARD,
		SCORE_A_E_EASY,
		SCORE_A_E_NORMAL,
		SCORE_A_E_HARD,
		SCORE_B_B_EASY,
		SCORE_B_B_NORMAL,
		SCORE_B_B_HARD,
		SCORE_B_E_EASY,
		SCORE_B_E_NORMAL,
		SCORE_B_E_HARD,
		SCORE_C_B_EASY,
		SCORE_C_B_NORMAL,
		SCORE_C_B_HARD,
		SCORE_C_E_EASY,
		SCORE_C_E_NORMAL,
		SCORE_C_E_HARD,
		SCORE_MAX
	};
private:
	struct Scoretable
	{
		unsigned long long m_score;
		unsigned long long m_combo;
		unsigned int m_stage;
		std::string m_name;

		bool operator<(const Scoretable& a) const
		{
			return this->m_score < a.m_score;
		}
		bool operator>(const Scoretable& a) const
		{
			return this->m_score > a.m_score;
		}
	};

	std::vector<Scoretable> m_scoretable[ScoreEntry::SCORE_MAX];
public:
	Scoreboard()
	{
		for (int i = 0; i < 3; i++)
		{
			for (int i = 0; i < 10+1; i++)
			m_scoretable[i].push_back(Scoretable{50000000ULL - (i*1000000ULL), 1, 1, "MTE"});
		}
	}
	double set_scoreentry(double entry, const char* score, const char *combo, double stage, const char *name)
	{
		m_scoretable[(int)(entry)][m_scoretable[(int)(entry)].size() - 1] =
		{ std::stoull(score), std::stoull(combo), (unsigned int)(stage), name };
		return 0;
	}
	double initialize_scoreentry(double entry, double pos, const char* score, const char* combo, double stage, const char* name)
	{
		m_scoretable[(int)(entry)][pos] =
		{ std::stoull(score), std::stoull(combo), (unsigned int)(stage), name };
		return 0;
	}
	const char* get_score(double entry, double pos)
	{
		std::string score = std::to_string(m_scoretable[(int)(entry)][(pos)].m_score);
		const char* score_c = score.c_str();
		return score_c;
	}
	const char* get_combo(double entry, double pos)
	{
		std::string combo = std::to_string(m_scoretable[(int)(entry)][(pos)].m_combo);
		const char* combo_c = combo.c_str();
		return combo_c;
	}
	double get_stage(double entry, double pos)
	{
		return m_scoretable[(int)(entry)][(pos)].m_stage;
	}
	const char* get_name(double entry, double pos)
	{
		return m_scoretable[(int)(entry)][(pos)].m_name.c_str();
	}
};
