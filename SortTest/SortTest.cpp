﻿
#include <iostream>
#include <algorithm>
#include <vector>

// 순차 탐색
int linear_search(int *data, int n, int target)
{
    for (int i = 0; i < n; i++)
    {
        if (data[i] == target)
        {
            return i;
        }
    }
    return -1;
}

// 이진 탐색
int binary_search(int *data, int n, int target)
{
    int lower = 0;
    int upper = n - 1;
    while (lower <= upper)
    {
        int mid = (lower + upper) / 2;

        if (data[mid] == target)
        {
            return mid;
        }
        else
        {
            if (data[mid] < target) // 정답이 mid의 오른쪽에 있음
            {
                lower = mid + 1; // 검색범위 재설정
            }
            else // 정답이 mid의 왼쪽에 있음
            {
                upper = mid - 1; // 검색범위 재설정
            }
        }
    }
    return -1;
}

int main()
{
    int data[] = { 1, 2, 3, 5, 7, 10, 13, 15, 18, 23, 25, 27, 30, 32, 33 };
    int target = 30;

    int res = linear_search(data, std::size(data), target);
    std::cout << res << std::endl;

    int res2 = binary_search(data, std::size(data), target);
    std::cout << res2 << std::endl;

    bool res3 = std::binary_search(std::begin(data), std::end(data), target);
    std::cout << res3 << std::endl;
}

/*
bool abs_cmp(const int a, const int b)
{
    return std::abs(a) < std::abs(b);
}

struct AbsCmp
{
    bool operator()(int a, int b)
    {
        return std::abs(a) < std::abs(b);
    }
};

int main()
{
    // 배열의 정렬
    int arr[5] = { 4, 2, 3, 5, 1 };
    //std::sort(arr, arr + 5);
    //std::sort(std::begin(arr), std::end(arr));
    //std::sort(std::begin(arr), std::end(arr), std::greater<int>()); // 내림차순
    std::sort(std::begin(arr), std::end(arr), std::less<int>()); // 오름차순

    for (auto a : arr)
    {
        std::cout << a << ", ";
    }
    std::cout << std::endl;

    // 백터 정렬
    std::vector<std::string> vec = { "orange", "banana", "apple", "lemon" };
    //std::sort(vec.begin(), vec.end());
    std::sort(vec.begin(), vec.end(), std::greater<>()); // 내림차순

    for (auto &a : vec)
    {
        std::cout << a << ", ";
    }
    std::cout << std::endl;

    // 정렬 비교 방법 지정 : 절대값의 오름차순 : 2, -3, 5, 7, 10
    std::vector<int> nums = { 10, 2, -3, 5, 7 };
    // 함수 포인터
    //std::sort(nums.begin(), nums.end(), abs_cmp);
    // 함수 객체
    //std::sort(nums.begin(), nums.end(), AbsCmp());
    // 람다 표현식
    std::stable_sort(nums.begin(), nums.end(), [](int a, int b) -> bool { return std::abs(a) < std::abs(b); });
    for (auto& a : nums)
    {
        std::cout << a << ", ";
    }
    std::cout << std::endl;

}
*/
/*
// 정수형 데이터 오름차순 정렬
// 버블정렬
void bubble_sort(int *data, int n) // O(n2)
{
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = n - 1; j > i; j--)
        {
            // 인접한 원소끼리 비교
            if (data[j] < data[j - 1])
            {
                std::swap<int>(data[j], data[j - 1]);
            }
        }
    }
}

// 선택정렬
void selection_sort(int *data, int n) // O(n2)
{
    for (int i = 0; i < n - 1; i++)
    {
        // 최소값을 가지고있는 원소의 위치
        int minidx = i;
        for (int j = i + 1; j < n; j++)
        {
            if (data[j] < data[minidx])
            {
                minidx = j;
            }
        }
        std::swap<int>(data[i], data[minidx]);
    }
}

// 삽입정렬
void insertion_sort(int *data, int n) // O(n2)
{
    for (int i = 1; i < n; i++)
    {
        int key = data[i];
        int j = i - 1;
        while (j >= 0 && data[j] > key)
        {
            data[j + 1] = data[j];
            j--;
        }
        data[j + 1] = key;
    }
}

// 병합정렬
void merge(int *buff, int* data, int left, int mid, int right) // 결합
{
    int i = left;
    int j = mid + 1;
    int k = left;

    while (i <= mid && j <= right)
    {
        //if (data[i] < data[j]) // 오름차순
        if (data[i] > data[j]) // 내림차순
        {
            buff[k++] = data[i++];
        }
        else
        {
            buff[k++] = data[j++];
        }
    }
    // 나머지 부분 처리
    while (i <= mid)
    {
        buff[k++] = data[i++];
    }
    while (j <= right)
    {
        buff[k++] = data[j++];
    }
    // 임시 버퍼에 있는 정렬된 데이터를 data에 다시 저장
    for (int x = left; x <= right; x++)
    {
        data[x] = buff[x];
    }
}
void merge_sort(int* data, int left, int right)
{
    int buff[257];
    if (left < right)
    {
        int mid = (left + right) / 2;
        merge_sort(data, left, mid);
        merge_sort(data, mid + 1, right);
        merge(buff, data, left, mid, right);
    }
}

// 퀵정렬
int partition(int *data, int left, int right)
{
    int pivot = data[left];
    int i = left + 1;
    int j = right;

    while (true)
    {
        while (data[i] <= pivot && i < right)
        {
            i++;
        }

        while (data[j] > pivot && j > left)
        {
            j--;
        }
        if (i < j)
        {
            std::swap<int>(data[i], data[j]);
        }
        else
        {
            break;
        }
    }
    std::swap<int>(data[left], data[j]);
    return j;
}
void quick_sort(int *data, int left, int right)
{
    if (left < right)
    {
        int p = partition(data, left, right);
        quick_sort(data, left, p - 1);
        quick_sort(data, p + 1, right);
    }
}

int main()
{
    int data[9] = { 2, 6, 7, 4, 1, 8, 5, 3, 9 };
    //bubble_sort(data, 9);
    //selection_sort(data, 9);
    //insertion_sort(data, 9);
    merge_sort(data, 0, 8);
    //quick_sort(data, 0, 8);

    for (auto e : data)
    {
        std::cout << e << ", ";
    }
    std::cout << std::endl;
}
*/